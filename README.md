# Tumblr Downloader

This is a small utility group that lets you download all of the images / videos from a given Tumblr with a minimal install environment.

required tools are: 

 - jq
 - Unix shell env

To get a tumblr's details:

`./scrape.sh <tumblr_name>`

Note that tumblr_name is without the suffix `tumblr.com`, so for a tumblr such as `foobar.tumblr.com` you would put:

`./scrape.sh foobar`

This downloads _all_ of the blogs posts to your computer. Once that has happened, you can use the `photo.sh` or `video.sh` to download
all of the URLs for the respective post type:

`./photo.sh scraped/foobar.json`
`./video.sh scraped/foobar.json`

Will create a directory called `foobar/` with a `videos.txt` or `photos.txt` which is a list of all of the *valid* urls that the blog has.

You can then use these files to download with a simple one-liner like:

`cat photos.txt | xargs -i{} curl -SsLO "{}"`

to download all of the photos.

There is also a script called `get-reblog-blogs.sh` which will go through a blogs posts and give a list of all the posts that were reblogged
from other blogs.

`./get-reblog-blogs.sh scraped/foobar.json`
