#!/bin/bash

## import the tumblr api
. ./tumblr.sh

## import utils
. ./util.sh

## Define the api key used to make requests to Tumblr
API_KEY=$(cat .api_key)

function work {
  local blog=$1
  local type=${TYPE:-video}
	local path=$(echo "$blog" | sed -E 's/[-\/]/_/g')

  # create relevant directories.
  create-dirs $path scraped 
  # status_codes status_codes/200 status_codes/403

  ## Create a file @ scraped/$blog.json with an array of all $type posts
  RESULT=$(get-all-posts $blog $type $path)

	if [ $? -ne 0 ]; then
		echo "Download of $blog.tumblr.com finished with errors."
	else
		echo "Completed download of $blog.tumblr.com."
	fi

}

function main {
	if [ -f "$1" ]; then
		TUMBLRS=($(cat "$1"))
		for TUMBLR in ${TUMBLRS[@]}; do
			work "$TUMBLR"
		done
	else
		work "$1"
	fi
}

main "$@"
