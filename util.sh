#!/usr/bin/env bash

## Create a directory if it doesn't exist already
## $1 - The name of the directory to create
function create-dir {
    if [ ! -d "$1" ]; then
        echo "Creating $1 directory"
        mkdir -p "$1"
    else
        echo "Directory $1 already exists"
    fi
}
## loop shortcut for create-dir
## $@ - All names of dirs to create
function create-dirs {
  for var in "$@"; do
    create-dir $var
  done
}
