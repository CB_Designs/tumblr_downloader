#!/usr/bin/env bash

TUMBLR_NAME=$(echo $1 | rev | cut -d/ -f1 | rev | cut -d. -f1)

main() {
	mkdir -p "$TUMBLR_NAME"
jq --raw-output '[.[] | .reblogged_root_uuid, .reblogged_from_uuid] | flatten | unique | .[] | select(. != null) | select(. | contains("deactivated")|not) | rtrimstr(".tumblr.com")' $1 > "$TUMBLR_NAME"/reblogged.txt
}

main "$@"
