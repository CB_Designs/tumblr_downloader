#!/usr/bin/env bash

## Build a Tumblr api url
## $1 - The name of the blog
## $2 - The slug of the api endpoint
## $@ - Any additional querystring params
function build-api-url {
    local blog=$1; shift
    local slug=$1; shift
    if [ -z "$1" ]; then
        echo "https://api.tumblr.com/v2/blog/$blog/$slug/?api_key=$API_KEY"
    else
        echo "https://api.tumblr.com/v2/blog/$blog/$slug/?api_key=$API_KEY&${@}"
    fi
}

## Issue a curl request
## $1 - The url to request
function do-curl-request {
    curl -sS $1
}

## get the posts of a blog
## $1 - the name of the blog to fetch posts from
## $2 - A querystring formatted string to append to the url
function get-blog-posts {
    local url=$(build-api-url $1 "posts" $2)
    echo $(do-curl-request ${url})
}

## Get the posts of a blog by a certain type and offset
## $1 - The name of the blog to fetch the posts from
## $2 - The type of post to fetch (video, image)
## $3 - The offset of posts to fetch
function get-blog-posts-by-type-and-offset {
    local query="type=${2}&offset=${3}"
    local url=$(build-api-url $1 "posts" ${query})
    echo $(do-curl-request ${url})
     
}

## Get the posts of a blog by an offset
## $1 - The name of the blog to fetch the posts from
## $3 - The offset of posts to fetch
function get-blog-posts-by-offset {
    local query="offset=${2}&notes_info=true&reblog_info=true"
    local url=$(build-api-url $1 "posts" ${query})
    echo $(do-curl-request ${url})
}

## Get the information end point of a blog
## $1 - The name of the blog to fetch
## $2 - Any additional querystring params
function get-blog-info {
    local url=$(build-api-url $1 "info" $2)
    echo $(do-curl-request ${url})
}

## Get the total post count of a blog
## $1 - The name of the blog
function get-blog-post-count {
  local result=$(get-blog-info $1)
  echo "$result" | jq -r '.response.blog.total_posts'
}

## Get the total post count of a blog filtered by a certain type
## $1 - The name of the blog
## $2 - The type to filter by
function get-blog-post-count-by-type {
    local result=$(get-blog-posts $1 "type=$2")
    echo "$result" | jq -r '.response.total_posts'
}

## Create an array with the offsets of a blogs posts filtered by type
## $1 - The name of the blog
## $2 - The type to filter by
function get-offset-array {
    local post_count=$(get-blog-post-count $@)
    local i=0
    local offsets=()
    while ((i < ${post_count})); do
        offsets[$i]=$i
        ((i += 20))
    done
    echo "${offsets[@]}"
}


## Concatenate all of the post files for a blog together
## $1 - The name of the blog
function slurp-posts {
  jq -s '[.[].response.posts[]]' "$1"/*.json > "scraped/$1.json"
  # Remove the offsets folder
  rm -rf "$1"
}

## verify that a blog exists and is accessible
## $1 - The target blog
function verify-blog {
	URL=$(build-api-url "$1" info)
	echo $(curl -s -o /dev/null -w "%{http_code}" "$URL")	

}

## Fetch all of the posts for a given blog
## $1 - The name of the blog
## $2 - The type of post to fetch
## $3 - Cleaned name of blog for directories
function get-all-posts {
		STATUS=$(verify-blog "$1")
		if [ "$STATUS" != 200 ]; then
			echo "Attempting to contact Blog URL resulted in a $STATUS error code."
			rm -rf "$1"
			exit 1
		else 	
			echo $(get-blog-info $1) | jq '.response.blog.updated' | xargs -I{} date -r {}
			local offsets=($(get-offset-array $1))
			local y=1
			local loop=0
			local batch_size=40
			echo $(date)
			(
				for offset in ${offsets[@]}; do
						((i=i%batch_size)); ((i++==0)) && wait && sleep 60 
						printf "Getting ${y}/${#offsets[@]} posts request\r"
						echo "$(get-blog-posts-by-offset $1 $offset)" > "$3/$offset.json" &
						((y++))
				done
				wait
			)
			echo -e "\nFinished downloading"
			slurp-posts $3
			exit 0
	fi
}

