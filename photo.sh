#!/usr/bin/env bash

. ./util.sh

JSON_FILE="$1"
TUMBLR_NAME=$(echo "$1" | rev | cut -d/ -f1 | rev | cut -d. -f1)

## Get the id of a post url
## $1 - The url to get the id of.
function get-post-id {
    local id=$(echo "$1" | sed "s/.*\tumblr_\(.*\)\..*/\1/")
    echo "$id"
}

main() {
	create-dirs status_codes/200 status_codes/403 "$TUMBLR_NAME"
	$(jq -r 'map(select(.type == "photo")) | .[] |= .photos[0].original_size.url' "$JSON_FILE" > "$TUMBLR_NAME"/photo.json)
  local url_array=($(jq -r 'map(select(.type == "photo")) | .[] |= .photos[0].original_size.url | @tsv' $JSON_FILE
))
  local batch_size=50
  local y=0
	echo "Starting fetch"
  (
      for url in ${url_array[@]}; do
          ((i=i%batch_size)); ((i++==0)) && wait
          local id=$(get-post-id $url)
          ((y++))
          printf "Getting ${y}/${#url_array[@]} status codes\r"
          curl --max-time 2 -i -o - -X HEAD -s $url | grep HTTP | RES=$(awk '{print $2}') > status_codes/$RES/$id &
      done
      wait
  )
  echo -e "\nFetched status codes"

  ## Expand this to be more than just 403 (500 504 3xx)
  local erroneous_ids=($(find status_codes/403 -type f | rev | cut -d'/' -f1 | rev))
  for id in ${erroneous_ids[@]}; do
      tmp=$(mktemp)
      jq '[ .[] | select(contains("'$id'")|not) ]' "$TUMBLR_NAME/photo.json" > $tmp
      mv $tmp "$TUMBLR_NAME/photo.json"
  done
	
	TOTAL=$(jq 'length' "$TUMBLR_NAME"/photo.json)
	echo "${#url_array[@]} urls checked $((${#url_array[@]} - $TOTAL)) were invalid"	
  rm -rf status_codes
}

main 
